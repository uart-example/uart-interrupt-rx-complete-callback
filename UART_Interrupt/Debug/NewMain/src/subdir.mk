################################################################################
# Automatically-generated file. Do not edit!
# Toolchain: GNU Tools for STM32 (10.3-2021.10)
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../NewMain/src/NewMain.c 

OBJS += \
./NewMain/src/NewMain.o 

C_DEPS += \
./NewMain/src/NewMain.d 


# Each subdirectory must supply rules for building sources it contributes
NewMain/src/%.o NewMain/src/%.su: ../NewMain/src/%.c NewMain/src/subdir.mk
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DDEBUG -DUSE_HAL_DRIVER -DSTM32F303x8 -c -I../Core/Inc -I../Drivers/STM32F3xx_HAL_Driver/Inc -I../Drivers/STM32F3xx_HAL_Driver/Inc/Legacy -I../Drivers/CMSIS/Device/ST/STM32F3xx/Include -I../Drivers/CMSIS/Include -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"

clean: clean-NewMain-2f-src

clean-NewMain-2f-src:
	-$(RM) ./NewMain/src/NewMain.d ./NewMain/src/NewMain.o ./NewMain/src/NewMain.su

.PHONY: clean-NewMain-2f-src

