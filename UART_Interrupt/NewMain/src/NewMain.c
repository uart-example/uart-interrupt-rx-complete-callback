/*
 * NewMain.c
 *
 *  Created on: Oct 26, 2022
 *      Author: krist
 */
#include "main.h"
#include "stm32f3xx_hal.h"
#include <string.h>
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>





extern UART_HandleTypeDef huart2;
//transmit and receive data
uint8_t Data[8] = {0};
uint8_t i = 0;
uint8_t checkSum = 0;




void APP_NewMain()
{

	char works [] = "Application works\r\n";



	HAL_UART_Transmit(&huart2, (uint8_t*)works, strlen(works), 100);
	HAL_UART_Receive_IT (&huart2, Data, 1);




	while(1);

}

void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
{

	if((115 == Data[0] || 83 == Data[0])&& isalpha(Data[0]))
	{
		//LED indicates that the Start byte has been send
		HAL_GPIO_TogglePin(GREEN_LED_GPIO_Port, GREEN_LED_Pin);
		//Receives the rest of the bytes
		HAL_UART_Receive(huart, Data + 1, 5, 100);
	}

	for(i = 1; i < 5 ; i++)
	{
		if(isalpha(Data[i]))
		{
			Data[i] = tolower(Data[i]);
		}
		if(Data[i] >= 97 && Data[i] <= 104 && isalpha(Data[i]))
		{
			//TO something
			HAL_GPIO_TogglePin(RED_LED_GPIO_Port, RED_LED_Pin);



		}
		else if(Data[i] >= 49 && Data[i] <= 56 && isdigit(Data[i]))
		{
			//TO something
			HAL_GPIO_TogglePin(RG_LED_GPIO_Port, RG_LED_Pin);


		}


	}
	//checksum
	for(i = 0; i < 7 ; i++)
	{
		checkSum += Data[i];

	}

	Data[6] = checkSum;


	HAL_UART_Transmit(huart, Data, 7, 100);
	HAL_UART_Receive_IT(huart, Data, 1);

}



